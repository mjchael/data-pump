package com.mjchael.datapump.cli.command;

import com.mjchael.datapump.cli.strategy.CommandLineStrategy;
import com.mjchael.datapump.core.service.DataPumpService;

import java.util.logging.Level;
import java.util.logging.Logger;


public class SingleTableExportCommand implements CommandLineStrategy {

    private DataPumpService dataPumpService;
    private static final Logger LOGGER = Logger.getLogger(SingleTableExportCommand.class.getName());


    public SingleTableExportCommand(DataPumpService dataPumpService){
        this.dataPumpService = dataPumpService;
    }

    @Override
    public void executeCommand(String... args) {
        LOGGER.log(Level.INFO, "Start Export");
        String owner = args[0].toUpperCase();
        String tableName = args[1].toUpperCase();
        String whereClause = args[2];

        String insertStatement = dataPumpService.generateInsert(
                owner,
                tableName,
                whereClause
        );
        String deleteStatement = dataPumpService.generateDelete(tableName, whereClause);
        dataPumpService.writeInFile(
                generateFileName(tableName),
                deleteStatement.concat(insertStatement)
        );
        LOGGER.log(Level.INFO, "End Export");
    }

    private String generateFileName(String tableName){
        return "insert_"+tableName+".sql";
    }
}
