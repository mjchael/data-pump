package com.mjchael.datapump.cli.command;

import com.mjchael.datapump.cli.strategy.CommandLineStrategy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
public class VersionCommand implements CommandLineStrategy {

//    ${info.app.name} v(${info.app.version})
    private String version;

    @Override
    public void executeCommand(String... args) {
        System.out.println("0.0.1");
    }
}
