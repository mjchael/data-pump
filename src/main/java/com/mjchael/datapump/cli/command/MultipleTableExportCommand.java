package com.mjchael.datapump.cli.command;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.mjchael.datapump.cli.strategy.CommandLineStrategy;
import com.mjchael.datapump.core.config.ExportTableModel;
import com.mjchael.datapump.core.config.ExportTableSpecification;
import com.mjchael.datapump.core.service.DataPumpService;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class MultipleTableExportCommand implements CommandLineStrategy {

    private DataPumpService dataPumpService;
    private static final Logger LOGGER = Logger.getLogger(MultipleTableExportCommand.class.getName());

    public MultipleTableExportCommand(DataPumpService dataPumpService){
        this.dataPumpService = dataPumpService;
    }

    @Override
    public void executeCommand(String... args) {
        LOGGER.log(Level.INFO, "Start Export");

        try {
            File fileRead = new File(args[0]);
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            ExportTableModel objectExportDefinition
                    = mapper.readValue(fileRead, ExportTableModel.class);

            Map<String, List<ExportTableSpecification>> data =
                    objectExportDefinition
                            .getTableToExport()
                            .stream()
                            .collect(Collectors.groupingBy(ExportTableSpecification::getTableName));
            dataPumpService.writeInFile(fileRead.getName().replace(".yml", ".sql"), objectExportDefinition.getOwner(), data);

        } catch ( IOException e){
            LOGGER.log(Level.SEVERE, "Error reading data "+ e.getMessage(), e);
        }

        LOGGER.log(Level.INFO, "End Export");
    }
}
