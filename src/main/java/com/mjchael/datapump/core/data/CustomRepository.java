package com.mjchael.datapump.core.data;

import org.springframework.data.jpa.repository.QueryHints;

import java.util.List;

public interface CustomRepository {

    @QueryHints(@javax.persistence.QueryHint(name="org.hibernate.fetchSize", value = "10000"))
    List<Object[]> executeQuery(String sqlStatement);
}
