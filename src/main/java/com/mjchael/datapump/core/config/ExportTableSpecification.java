package com.mjchael.datapump.core.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * {@code ExportTableSpecification} class represents the export tables info
 *
 * @author michael
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExportTableSpecification{
    private String tableName;
    private String whereClause;
    private String wiki;
}