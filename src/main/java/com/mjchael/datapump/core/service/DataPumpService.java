package com.mjchael.datapump.core.service;

import com.mjchael.datapump.core.config.ExportTableSpecification;
import com.mjchael.datapump.core.model.AllTableColumn;
import com.mjchael.datapump.util.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class DataPumpService {
    @Autowired
    public AllTableColumnService allTableColumnService;
    @Autowired
    public CustomQueryService customQueryService;

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    private static final Logger LOGGER = Logger.getLogger(DataPumpService.class.getName());



    public String generateInsert(String owner, String tableName, String whereClause) {
        List<AllTableColumn> list = allTableColumnService.findByTableNameAndOwner(tableName.toUpperCase(), owner.toUpperCase());
        list.sort(Comparator.comparing(AllTableColumn::getColumnId));
        List<Object[]> dataList=  customQueryService.executeQuery(generateStringQuery(list, whereClause));
        return "\n--insert into "+ tableName + buildInsertStatement(dataList, list, tableName);
    }

    public String generateDelete(String tableName, String whereClause){
        return "\n-- delete "+ tableName +" \ndelete from " + tableName + " where " + whereClause + ";";
    }

    public void writeInFile(String fileName, String statements){
        try {
            Path outFolder = Path.of("out");

            if (Files.notExists(outFolder)) Files.createDirectory(outFolder);

            Path file = outFolder.resolve(fileName);

            if (Files.exists(file)) Files.delete(file);

            try (PrintWriter out = new PrintWriter(
                    new OutputStreamWriter(
                            Files.newOutputStream(file, StandardOpenOption.CREATE),
                            StandardCharsets.UTF_8))){

                out.append(statements);
            }
        } catch (IOException io) {
            LOGGER.log(Level.SEVERE, "Error writing data "+ io.getMessage(), io);
        }
    }

    public void writeInFile(String fileName, String owner, Map<String, List<ExportTableSpecification>> tableExportConfig){
        Map<String, String> tableExportQueries = new HashMap<>();
        List<String> deleteStatements = new ArrayList<>();
        for (String tableName : tableExportConfig.keySet()){
            String insertStatements = generateInsert(owner, tableName, tableExportConfig.get(tableName).stream().findFirst().orElseThrow().getWhereClause());
            String deleteStatement = generateDelete(tableName, tableExportConfig.get(tableName).stream().findFirst().orElseThrow().getWhereClause());
            deleteStatements.add(deleteStatement);
            tableExportQueries.put(tableName, insertStatements);
        }

        writeInFile(fileName,
                String.join("", deleteStatements)
                        .concat("\n")
                        .concat(tableExportQueries.values().stream().collect(Collectors.joining("\n"))));

    }

    private String buildInsertStatement(List<Object[]> dataList, List<AllTableColumn> list,  String tableName){
        return dataList.stream()
                .parallel()
                .map(r -> {
                    String insert =  "\ninsert into " + tableName + " (" + getColumnQuery(list) + ") values (";
                    Function<Object, Object> mapToBigDecimalString = (o) ->  o instanceof BigDecimal? o.toString():o;
                    Function<Object, Object> mapToStringCharacter = (o) ->  o instanceof String||o instanceof Character?  "'"+o+"'":o;
                    Function<Object, Object> mapToTimestampString = (o) ->  o instanceof Timestamp? formatDate(o):o;
                    Function<Object, Object> mapNullValues = (o) -> o == null? "null": o;
                    String values = Arrays.stream(r)
                            .map(mapToBigDecimalString
                                    .andThen(mapToStringCharacter)
                                    .andThen(mapToTimestampString)
                                    .andThen(mapNullValues)
                                    .andThen(Object::toString)
                            )
                            .reduce("", (a, b)-> a + "," + b);
                    insert += values;
                    insert += ");";
                    return insert;
                })
                .collect(Collectors.joining())
                .concat("\ncommit;");
    }

    private String formatDate(Object o){
        String datetime = ((Timestamp) o).toLocalDateTime().format(formatter);
        return SqlBuilder.buildDate(datetime);
    }

    /**
     * Generates an sql statement given the list of columns and the where clause
     * @param list all column list
     * @param whereClause where clause
     * @return String
     */
    private String generateStringQuery(List<AllTableColumn> list, String whereClause) {
        if (list.isEmpty()) {
            throw new AssertionError("Table does not exist");
        }
        if (whereClause.trim().isEmpty()) {
            throw new AssertionError("Where clause cannot be null");
        }

        return SqlBuilder.buildStatement(
                getColumnQuery((list)),
                list.stream()
                        .findFirst()
                        .map(AllTableColumn::getTableName)
                        .orElseThrow(),
                whereClause);
    }

    /**
     * Method that builds the list of column
     * @param list
     * @return String of {@link AllTableColumn column}
     */
    private String getColumnQuery(List<AllTableColumn> list) {
        return list
                .stream()
                .map(AllTableColumn::getColumnName)
                .reduce("",
                        (generatedQuery, column) -> (generatedQuery + column + " "),
                        String::concat)
                .trim()
                .replace(" ", ", ");
    }
}
