package com.mjchael.datapump.util;

public class SqlBuilder {

    public static String buildDate(String datetime){
        return "to_date('"
            + datetime
            +"', 'DD/MM/YYYY HH24:MI:SS')";
    }

    public static String buildStatement(String selectClause, String tableName, String whereClause){
        return "select " + selectClause + " from " + tableName + " where " + whereClause;
    }
}
