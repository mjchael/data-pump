# data-pump
[![pipeline status](https://gitlab.com/mjchael/data-pump/badges/master/pipeline.svg)](https://gitlab.com/mjchael/data-pump/-/commits/master)
[![coverage report](https://gitlab.com/mjchael/data-pump/badges/master/coverage.svg)](https://gitlab.com/mjchael/data-pump/-/commits/master)

# data-pump
Version 1.0.0

# Configuration
## Database connection
When running the commandline application, the user should specify the database connection:
> --db.connect=hr/hr@xe
> --db.connect=michael/michael@172.18.0.2:1521/ORCL

Update the username, password and url according to the database that you want to extract your data.

# Usage
```console
usage: java -jar data-pump.jar --db.connect=username/password@sid [run] [file] [version] [help] 
[run OPTIONS]
	 Option 1:
		 [<owner] [tableName] [whereClause]
[file OPTIONS]
	 Option 1:
		 [file.yml]
```
## YAML file format:
For option 2, the file.yml should be in the following format:

```yaml
owner: <table owner>
tableToExport:
  - tableName: <table name>
    whereClause: <where clause>
    wiki: <Some info>
  - tableName: <table name>
    whereClause: <where clause>
    wiki: <Some info>

```

## Run program for a specific table:
```console
java -jar data-pump.jar --db.connect=hr/hr@xe run HR JOBS 1=1
```

## Run program for a set of table:

```console
java -jar data-pump.jar --db.connect=hr/hr@xe file jobs.yml
```

jobs.yml:
```yaml
tableToExport:
  - tableName: employees
    whereClause: employee_id in (select manager_id from employees where employee_id = 101)
    wiki: "Master Table"
  - tableName: locations
    whereClause: 1=1
```


## Run program for a set of table:

```console
java -jar data-pump.jar --db.connect=hr/hr@xe file jobs.yml
```

jobs.yml:
```yaml
owner: michael
tableToExport:
  - tableName: michael
    whereClause: 1=1
    wiki: "Master Table"
  - tableName: test
    whereClause: 1=1 and id = 3
```

